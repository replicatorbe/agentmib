
package agentmib;

/**
 *
 * @author jerome fafchamps
 */
public class Config {

    /**
     * connection IRC Server with IRC nickname.
     */
    private static final String server = "irc.espace-irc.org";
    private static final String chan = "#services_infos";
    private static final String nickname = "AgentMIB";
    /**
     * password and u-line (see ircd.conf) *
     */
    private static final String pass = "test";
    private static final String link = "IRCBook.baboon.fr";
    
    /** SQL **/
    
    private static final String url = "jdbc:mysql://91.121.113.202:3306/security";
    private static final String user = "test";
    private static final String passwd = "test";
   
    
    /**
     * instance *
     */
    private static Config INSTANCE = null;

    private Config() {
    }

    /**
     * get *
     */
    public static synchronized Config getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Config();
        }
        return INSTANCE;
    }

    public String getServer() {
        return server;
    }

    public String getChan() {
        return chan;
    }

    public String getNickname() {
        return nickname;
    }

    public String getPass() {
        return pass;
    }

    public String getLink() {
        return link;
    }
    
    public static String getUrl() {
        return url;
    }

    public static String getUser() {
        return user;
    }

    public static String getPasswd() {
        return passwd;
    }
}
