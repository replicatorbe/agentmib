
package agentmib.connection;

/**
 *
 * @author jerome fafchamps
 */
import agentmib.Config;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnectionSql {

    /**
     * Information de connexion de base de donnÈe
     */
    private static Connection connect;

    public static Connection getInstance() {

        if (connect == null) {

            try {
                Class.forName("com.mysql.jdbc.Driver");
                connect = DriverManager.getConnection(Config.getUrl(), Config.getUser(), Config.getPasswd());

            } catch (SQLException e) {
                System.out.println("Your database is down.");
                System.exit(0);
            } catch (ClassNotFoundException e1) {
                System.out.println("This class doens't exist.");
                System.exit(0);
            }
        }
        return connect;
    }

    public static boolean isValid(Connection connection) {
        if (connection == null) {
            return false;
        }
        ResultSet ping = null;
        try {
            if (connection.isClosed()) {
                return false;
            }
            ping = connection.createStatement().executeQuery("SELECT 1");
            return ping.next();
        } catch (SQLException sqle) {
            return false;
        } finally {
            if (ping != null) {
                try {
                    ping.close();
                } catch (Exception e) {
                }
            }
        }
    }
}