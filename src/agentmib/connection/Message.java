
package agentmib.connection;

/**
 *
 * @author jerome fafchamps
 */
public class Message {

    private String nick;
    private String host;

    public Message(String nick, String host) {
        this.nick = nick;
        this.host = host;

        System.out.println(nick + host);
    }

    public String getNick() {
        return nick;
    }

    public String getHost() {
        return host;
    }
}
