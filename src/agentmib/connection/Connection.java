
package agentmib.connection;

import agentmib.MIB;
import agentmib.Config;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jerome fafchamps
 */
public class Connection implements Runnable {

    private Socket socket = null;
    private PrintWriter out = null;
    private BufferedReader in = null;

    @Override
    public void run() {

             String line = null;
             
        try {

            socket = new Socket(Config.getInstance().getServer(), 7777);
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            out.println("PROTOCTL NICKv2 SJOIN2 UMODE2 NOQUIT VL TKLEXT");
            out.println("PASS " + Config.getInstance().getPass());
            out.println("SERVER " + Config.getInstance().getLink() + " 1 :AgentMIB");
            out.println(":" + Config.getInstance().getLink() + " SQLINE " + Config.getInstance().getNickname() + ":AgentMIB");
            out.println(":" + Config.getInstance().getLink() + " NICK " + Config.getInstance().getNickname() + " 1 5151547 AgentMIB " + Config.getInstance().getLink() + " 0 +oqS" + Config.getInstance().getLink() + " :AgentMIB");
            out.println(":" + Config.getInstance().getLink() + " EOS");
            out.println(":MODE " + Config.getInstance().getNickname() + "+O");
            out.println(":" + Config.getInstance().getNickname() + " JOIN " + Config.getInstance().getChan());

            while ((line = in.readLine()) != null) {

                String[] configuration = new String[300];
                configuration = line.split(" ");
                // System.out.println(line.toString());

                if (line.indexOf("PING") == 0) {
                    String s[] = line.split(":");
                    out.println("PONG :" + s[1]);

                } else if (line.indexOf("NICK") == 0) {

                    /**
                     * action g-line *
                     */
                    if (!new MIB().check(new Message(configuration[1], configuration[5]))) {
                        System.out.println("NOT DETECTION" + configuration[5]);
                        long unixTime = System.currentTimeMillis() / 1000;
                        long unixTime1 = unixTime + 3000;
                        //  out.println(":" + Config.getInstance().getLink() + " TKL + Z * " + configuration[5] + " AgentMIB " + unixTime1 + " " + unixTime + " :Not allowed");

                    }

                }
            }
        } catch (UnknownHostException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);

        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);

        } catch (SQLException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
