
package agentmib;

/**
 *
 * @author jerome fafchamps
 */
import agentmib.connection.ConnectionSql;
import agentmib.connection.Message;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class MIB {

    public Connection connect = ConnectionSql.getInstance();


    public boolean check(Message host) throws SQLException {


        try {

            InetAddress ip = InetAddress.getByName(host.getHost());
            String ip1 = ip.getHostAddress();

            Statement stmt = connect.createStatement();
            String queryString = "select count(*) from security.checkip where ip = '" + ip1 + "'";
        
            ResultSet rs = stmt.executeQuery(queryString);
            rs.next();
            
            int i = Integer.parseInt(rs.getString(1));
            if (i >= 1) {
                return true;
            }


        } catch (Exception e) {
            //
        }

        return false;

    }
}
